FROM ubuntu:14.04
RUN mkdir /setup

COPY data/setup-a data/packages /setup/
RUN /setup/setup-a

COPY data/setup-b data/ssh /setup/
COPY data/ssh /setup/ssh
RUN sudo -Hsu www-data /setup/setup-b

COPY data/setup-c data/patch /setup/
RUN service mysql start && sudo -Hsu www-data /setup/setup-c

RUN cd ~www-data/public && git log -1

ARG PHP_VERSION
COPY data/setup-d-before /setup/
RUN /setup/setup-d-before

ARG RELEASE_TAG
COPY data/setup-d data/parameters.yml /setup/
RUN service mysql start && sudo -EHsu www-data /setup/setup-d
